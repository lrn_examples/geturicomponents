package com.mycompany.myproject.forwarding;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/my/forwarding/servlet/*")
public class ForwardingServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// original client request URI is cleared 
		// and http://localhost:8080/MyApp + getRequestDispatcher()'s arg is used instead 
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/jsp/info.jsp");
		
		rd.forward(req, resp);
		
	}

	
	
}
