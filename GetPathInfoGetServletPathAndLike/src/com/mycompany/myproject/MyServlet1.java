package com.mycompany.myproject;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// DEMOSTRATES

//getRequestURL();   getRequestURI();
//
//getContextPath();  getServletPath();  getPathInfo();
//	
//getQueryString();  

// without "/*", servlet won't be triggered with 
// localhost:8080/path/to/servlet/legal/notes/my13.html
// (404 Error instead)
@WebServlet("/path/to/servlet/*")
public class MyServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
 

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// NOT String!!! StringBuffer getRequestURL()
		StringBuffer requestURL = request.getRequestURL();
		
		String requestURI = request.getRequestURI();
		
		// *************************************************
		
		// aka: Root Context, Root Path, (Web) App Path / Context
		String contextPath = request.getContextPath();
				
		// ALWAYS starts with "/"
		String servletPath = request.getServletPath();
		
		// extra path information 
		String pathInfo = request.getPathInfo();
		
		// *************************************************
		
		String queryString = request.getQueryString();
		
		// *************************************************
		
		// Fragment NOT SENT TO SERVER - NO WAY TO GET IT!
		// String fragment;			
		
		
		// out writes to Http Response Body
		PrintWriter out = response.getWriter();
		
		out.write("getRequestURL():  " + requestURL);
		out.println();
		out.write("getRequestURI():  " + requestURI);
		out.println();
		
		out.write("getContextPath():  " + contextPath);
		out.println();
		out.write("getServletPath():  " + servletPath);
		out.println();
		out.write("getPathInfo():  " + pathInfo);
		out.println();
		
		out.write("getQueryString():  " + queryString);
		out.println();
		
	}

	


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
