<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	
	<%		
		StringBuffer requestURL = request.getRequestURL();

		String requestURI = request.getRequestURI();
		
		String contextPath = request.getContextPath();
		
		String servletPath = request.getServletPath();
		
		String pathInfo = request.getPathInfo();		

		String queryString = request.getQueryString();

		
		// out is  JspWriter (implicit object).
		//Servlet's response.getWriter() returns PrintWriter
		out.write("getRequestURL():  " + requestURL);
		out.println();
		out.write("<br>");
		out.write("getRequestURI():  " + requestURI);
		out.write("<br>");

		out.write("getContextPath():  " + contextPath);
		out.write("<br>");
		out.write("getServletPath():  " + servletPath);
		out.write("<br>");
		out.write("getPathInfo():  " + pathInfo);
		out.write("<br>");

		out.write("getQueryString():  " + queryString);
		out.println();
	%>


</body>
</html>